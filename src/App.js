import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// Apps 
import HomePage from "./Dashboard";
import Dashboard from "./Dashboard";
import Deposits from "./Deposits";
import Orders from "./Orders";
import Customers from "./Customers";

class App extends React.Component {
  render() {
    return (
      <div>
        <Router className="App">
          <React.Fragment>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/deposits" component={Deposits} />
              <Route path="/orders" component={Orders} />

              {/* Apps */}
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/customers" component={Customers} />

              {/* <Route path="*" component={NotFoundPage} /> */}
              <Route path="*" component={HomePage} />
            </Switch>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;  

// Admin theme downloaded from
// https://demos.creative-tim.com/paper-dashboard-react/#/dashboard
